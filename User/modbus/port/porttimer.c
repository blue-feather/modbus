/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id$
 */

/* ----------------------- Platform includes --------------------------------*/
#include "stm32f4xx.h"
#include "port.h"
/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"




uint32_t count = 0;
/* ----------------------- static functions ---------------------------------*/
static void prvvTIMERExpiredISR( void );


/* ----------------------- Start implementation -----------------------------*/
BOOL
xMBPortTimersInit( USHORT usTim1Timerout50us )
{
    uint32_t uiPrescaler = 0;
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;	
    if (usTim1Timerout50us == 0)
    {
		return FALSE;
    }
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
	uiPrescaler =  (SystemCoreClock/2 / 20000) - 1; 				// 定时器时钟/20KHz（20kHz~50us）
	    
	TIM_TimeBaseStructure.TIM_Period = usTim1Timerout50us;          //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	 计时50us
	TIM_TimeBaseStructure.TIM_Prescaler =uiPrescaler;               //设置用来作为TIMx时钟频率除数的预分频值  10Khz的计数频率  
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;         //设置时钟分割:
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;     //TIM向上计数模式
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);                 //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位

	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;             //TIMx中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 6;       //先占优先级6级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;              //从优先级0级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;                 //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);                                 //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器
 
    TIM_ARRPreloadConfig(TIM2, ENABLE);
    TIM_Cmd(TIM2, ENABLE);
    return TRUE;
}


inline void
vMBPortTimersEnable(  )
{
    /* Enable the timer with the timeout passed to xMBPortTimersInit( ) */
    TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
    TIM_SetCounter(TIM2,0);	
    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
    TIM_Cmd(TIM2, ENABLE);    
}

inline void
vMBPortTimersDisable(  )
{
    /* Disable any pending timers. */
    TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
    TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
    TIM_SetCounter(TIM2,0);	
    TIM_Cmd(TIM2, DISABLE);	
}

/* Create an ISR which is called whenever the timer has expired. This function
 * must then call pxMBPortCBTimerExpired( ) to notify the protocol stack that
 * the timer has expired.
 */
static void prvvTIMERExpiredISR( void )
{
    ( void )pxMBPortCBTimerExpired(  );
}


/*********************************************************************************
* 函数名 : void TIM2_IRQHandler(void)
* 功  能 : 定时器中断处理函数,这里必须跳转到  pxMBPortCBTimerExpired()  才能够为modbuss提供时间计数的功能
* 说  明 : 
* 入  参 : 
* 返  回 : 
* 设  计 : zq  					时  间 : 2020.5.14
* 修  改 :  					    时  间 :  
********************************************************************************/
void TIM2_IRQHandler(void)
{	
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
	{
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
		( void )prvvTIMERExpiredISR();
	}		
}
