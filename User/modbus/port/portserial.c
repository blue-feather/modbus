/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id$
 */

#include "port.h"
#include "stdio.h"
/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

/* ----------------------- Hardware includes ----------------------------------*/
#include "stm32f4xx_usart.h"

/* ----------------------- static functions ---------------------------------*/
static void prvvUARTTxReadyISR( void );
static void prvvUARTRxISR( void );

/* ----------------------- Start implementation -----------------------------*/
void
vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )
{
    /* If xRXEnable enable serial receive interrupts. If xTxENable enable
     * transmitter empty interrupts.
     */
       if(xRxEnable == ENABLE)
       {
           USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);                                   
       }
       else
       {           
            USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);     
           
       }  
       if(xTxEnable == ENABLE)
       {
           USART_ITConfig(USART1, USART_IT_TC, ENABLE);                             
       }
       else
       {
            USART_ITConfig(USART1, USART_IT_TC, DISABLE);                                  
       }
}

BOOL
xMBPortSerialInit( UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity )
{
//    USART_InitTypeDef USART_InitStructure;
    switch (ucPORT)
    {
        case 1:
//            USART_InitStructure.USART_BaudRate = ulBaudRate;
//            USART_InitStructure.USART_WordLength = USART_WordLength_8b;        
//            USART_InitStructure.USART_Parity = USART_Parity_No;
//            USART_Init(USART1, &USART_InitStructure); 
//        	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);                                       //开启接受中断
//            USART_ITConfig(USART1, USART_IT_TC,   ENABLE);                                      //关闭发送完成中断
//            USART_ClearFlag(USART1, USART_FLAG_TC);                                              //清除发送中断
//            USART_Cmd(USART1, ENABLE);                                                           //使能 串口 1                                                                     //默认为接收模式
            break;
        default:
            break;
    }
    return TRUE;
}

BOOL
xMBPortSerialPutByte( CHAR ucByte )
{
    /* Put a byte in the UARTs transmit buffer. This function is called
     * by the protocol stack if pxMBFrameCBTransmitterEmpty( ) has been
     * called. */
    USART_SendData(USART1,ucByte);                                  //发送数据
    return TRUE;
}

BOOL
xMBPortSerialGetByte( CHAR * pucByte )
{
    /* Return the byte in the UARTs receive buffer. This function is called
     * by the protocol stack after pxMBFrameCBByteReceived( ) has been called.
     */
    uint16_t usiByte = 0;
    usiByte  = USART_ReceiveData(USART1);
    *pucByte = ((unsigned char)(usiByte & 0xFF));
    return TRUE;
}

/* Create an interrupt handler for the transmit buffer empty interrupt
 * (or an equivalent) for your target processor. This function should then
 * call pxMBFrameCBTransmitterEmpty( ) which tells the protocol stack that
 * a new character can be sent. The protocol stack will then call 
 * xMBPortSerialPutByte( ) to send the character.
 */
static void prvvUARTTxReadyISR( void )
{
    pxMBFrameCBTransmitterEmpty(  );
}

/* Create an interrupt handler for the receive interrupt for your target
 * processor. This function should then call pxMBFrameCBByteReceived( ). The
 * protocol stack will then call xMBPortSerialGetByte( ) to retrieve the
 * character.
 */
static void prvvUARTRxISR( void )
{
    pxMBFrameCBByteReceived(  );
}


void USART1_IRQHandler(void)
{
	if(USART_GetITStatus(USART1,USART_IT_RXNE)!=RESET)
	{		
         USART_ClearITPendingBit(USART1, USART_IT_RXNE);//清除中断标志位 
        prvvUARTRxISR(); //串口接收中断调用函数
       
        
	}	 

    //if(USART_GetITStatus(USART1, USART_IT_ORE) == SET)
    if(USART_GetFlagStatus(USART1, USART_FLAG_ORE) == SET)
    {  
        USART_ClearITPendingBit(USART1, USART_IT_ORE);
        prvvUARTRxISR(); 	//串口发送中断调用函数
    }

  
    if(USART_GetITStatus(USART1, USART_IT_TC) == SET)
    {
        USART_ClearITPendingBit(USART1, USART_IT_TC);//清除中断标志
        prvvUARTTxReadyISR();
    }
}	

