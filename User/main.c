/**
  ******************************************************************************
  * @file    main.c
  * @author  fire
  * @version V1.0
  * @date    2015-xx-xx
  * @brief   串口接发测试，串口接收到数据后马上回传。
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火  STM32 F407 开发板
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */

#include "stm32f4xx.h"
#include "./usart/bsp_debug_usart.h"
#include "./systick/bsp_SysTick.h"
#include "mb.h"
/**
  * @brief  主函数
  * @param  无
  * @retval 无
  */
int main(void)
{	
    SysTick_Init();
    /*初始化USART 配置模式为 115200 8-N-1，中断接收*/
    Debug_USART_Config();
	eMBInit(MB_RTU,1,1,115200,MB_PAR_NONE);
    eMBEnable(  );
    while(1)
	{	
		eMBPoll();
        
	}	
}



/*********************************************END OF FILE**********************/

